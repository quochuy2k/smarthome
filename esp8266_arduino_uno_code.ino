#include <WiFiEsp.h>
#include "dht.h"
#include <WiFiEspClient.h>
#include <WiFiEspUdp.h>
#include "SoftwareSerial.h"
#include <PubSubClient.h>
dht DHT;
char MqttServer[] = "broker.hivemq.com";
char ssid[] = "quochuy2k";           // your network SSID (name)
char pass[] = "10112000";           // your network password
int status = WL_IDLE_STATUS;   // the Wifi radio's status
#define DHTPIN 4     //Chân khai báo cảm biến nhiệt độ
#define DHTTYPE DHT11
// Initialize the Ethernet client object
WiFiEspClient espClient;

PubSubClient client(espClient);


SoftwareSerial soft(6, 7); // RX, TX cổng giao tiếp với esp8266
//time out
void setup() {
  // initialize serial for debugging
  Serial.begin(9600);
  // initialize serial for ESP module
  soft.begin(115200);
  // initialize ESP module
  WiFi.init(&soft);

  // check for the presence of the shield
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue
    while (true);
  }

  // attempt to connect to WiFi network
  while ( status != WL_CONNECTED) {
    Serial.print("Attempting to connect to WPA SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);
  }
  Serial.println("You're connected to the network");

  connect to MQTT server
    client.setServer(MqttServer, 1883);
    client.setCallback(callback);
  delay(100);
}

//print any message received for subscribed topic
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

void loop() {
    if (!client.connected()) {
      reconnect();
    }
    client.loop();
  Serial.println(WiFi.status());
  delay(100);
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect("arduinoClient")) {
      Serial.println("connected");
            DHT.read11(DHTPIN);
            // Read once about 250ms
            float h = DHT.humidity;
            float t = DHT.temperature;
            String temperature = String(t);
            String humidity = String(h);
            String payload = "{";
            payload += "\"temperature\":"; payload += temperature; payload += ",";
            payload += "\"humidity\":"; payload += humidity;
            payload += "}";
      
            char attributes[100];
            payload.toCharArray( attributes, 100 );
            client.publish("smarthouse/temparature", attributes);
            // ... and resubscribe
            client.subscribe("/smarthouse/stateled");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}
