import React from 'react'
import { View, ImageBackground } from 'react-native';
import { connect } from 'react-redux'
import * as actions from '../action/index'
import { StyleSheet } from 'react-native'
import LoginComponent from '../component/LoginComponent'
class LoginContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        }

    }
    componentDidMount() {

    }
    render() {
        return (
            <LoginComponent {...this.props} />

        )
    }
}
const styles = StyleSheet.create({

})
const mapStateToProps = (state) => {
    return {
        tempstate: state.tempstate.listState,
        user:state.userState.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTemperature: () => {
            dispatch(actions.getTemperature())
        },
        login:(data)=>{
            dispatch(actions.login(data))
        },
        register:(data)=>{
            dispatch(actions.register(data))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
