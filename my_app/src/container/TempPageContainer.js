import React from 'react'
import { View, ImageBackground } from 'react-native';
import { connect } from 'react-redux'
import * as actions from '../action/index'
import { StyleSheet } from 'react-native'
import TempComponent from '../component/TempComponent'
import HumComponent from '../component/HumComponent'
class TempContainer extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
        }
        this.handleStartStop = this.handleStartStop.bind(this);
    }
    componentDidMount() {
        this.props.getTemperature();
        this.handleStartStop()
    }
    handleStartStop() {
        const timer = setInterval(() => {
            this.props.getTemperature();
        }, 10000);
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/sky.jpg")}
                    style={{ width: 400, height: 800 }}
                >
                    <TempComponent {...this.props} />
                    <HumComponent {...this.props} />
                </ImageBackground>

            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
    }
})
const mapStateToProps = (state) => {
    return {
        tempstate: state.tempstate.listState
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getTemperature: () => {
            dispatch(actions.getTemperature())
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(TempContainer);
