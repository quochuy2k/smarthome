import React from 'react'
import { View, ImageBackground, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import * as actions from '../action/index'
import LedComponent from '../component/LedComponent'
import DoorComponent from '../component/DoorComponent'
import Icon from 'react-native-vector-icons/FontAwesome';
class HomeContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
        
    }

    render() {
        return (
            <View style={styles.container}>
                <ImageBackground
                    source={require("../../assets/bg-home2.jpg")}
                    style={{ width: 400, height: 800 }}
                >
                    <TouchableOpacity
                        style={styles.iconexit}
                       onPress={()=>{this.props.navigation.navigate('House')}}
                    >
                        <Icon name='angle-double-left' size={30} color='#FFC0CB' />
                    </TouchableOpacity>
                    <View style={styles.component}>
                        <LedComponent {...this.props} />
                        <DoorComponent {...this.props} />

                    </View>
                </ImageBackground>
            </View>

        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
    },
    component: {
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    iconexit: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        position: 'absolute',
        top: 40,
        left: 35,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 100,
    }
})
const mapStateToProps = (state) => {
    return {
        tempstate: state.tempstate.listState,
            
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addStateLed: (payload) => {
            dispatch(actions.assStateLed(payload))
        },
        addStateDoor: (payload) => {
            dispatch(actions.addStateDoor(payload))
        },
        updateAir: (payload) => {
            dispatch(actions.updateAir(payload))
        },
        deleteHome: (payload) => {
            dispatch(actions.delete_home(payload))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer);