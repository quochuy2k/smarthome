import React from 'react'
import { View, ImageBackground } from 'react-native';
import { connect } from 'react-redux'
import { StyleSheet } from 'react-native'
import HouseComponent from '../component/HouseComponent'
import * as actions from '../action/index'
class HouseContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    componentDidMount() {
    }
    render() {
        return (
            <View >
                <HouseComponent {...this.props} />
            </View>

        )
    }
}
const styles = StyleSheet.create({

})
const mapStateToProps = (state) => {
    return {
        user: state.userState.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
       addHome:(payload)=>{
           dispatch(actions.add_home(payload))
       },
       deleteHome:(payload)=>{
           dispatch(actions.delete_home(payload))
       }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(HouseContainer);