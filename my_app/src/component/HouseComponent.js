import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Modal,
  Pressable,
  TextInput,
  Alert
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { delete_home } from '../action';
export default function HouseComponent(props) {
  const [modalVisible, setModalVisible] = useState(false);
  const [codeHouse, setCodeHouse] = useState('')
  let house
  const detailHome = (id,data) => {
    if (id == '60ac72775320b70988150d12') {
      props.navigation.navigate('Home')
    }
    else{
      props.navigation.navigate('Form',{data:data})
    }
  }

  if (props.user.data.smart_home) {
    const deleteHouse = (id) => {
      let data = {
        idHome: id,
        idUser: props.user.data._id
      }
      props.deleteHome(data)
    }
    house = props.user.data.smart_home.map((element, key) => {
      return (
        <TouchableOpacity key={key} style={styles.buttonContainer} onPress={() => { detailHome(element._id,element) }}>
          <Text>{element.name_home}</Text>
          <TouchableOpacity
            style={styles.icon}
            onPress={() => { deleteHouse(element._id) }}
          >
            <Icon name='times-circle' size={30} color='#FFC0CB' />
          </TouchableOpacity>
        </TouchableOpacity>
      )
    })
  }

  const addHouse = () => {
    let data = {
      idHome: codeHouse,
      idUser: props.user.data._id
    }
    setModalVisible(!modalVisible)
    setCodeHouse('')
    props.addHome(data)
  }
  return (
    <View style={styles.container}>
      <View style={styles.header}></View>
      <Image style={styles.avatar} source={require("../../assets/home-logo.jpg")} />
      <View style={styles.body}>
        <View style={styles.bodyContent}>
          <Text style={styles.name}>{props.user.data.full_name}</Text>
          {house}
        </View>
      </View>
      <TouchableOpacity
        style={styles.icon}
        onPress={() => setModalVisible(true)}
      >
        <Icon name='plus' size={30} color='#FFC0CB' />
      </TouchableOpacity>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.inputView}>
              <TextInput
                style={styles.TextInput}
                placeholder="Code your home."
                placeholderTextColor="#003f5c"
                secureTextEntry={false}
                onChangeText={(code) => setCodeHouse(code)}
              />
            </View>
            <View style={styles.buttongroup}>
              <Pressable
                style={[styles.button, styles.buttonClose]}
                onPress={() => setModalVisible(!modalVisible)}
              >
                <Text style={styles.textStyle}>Close</Text>
              </Pressable>
              <Pressable
                style={[styles.button, styles.buttonSubmit]}
                onPress={() => addHouse()}
              >
                <Text style={styles.textStyle}>Submit</Text>
              </Pressable>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  buttongroup: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  header: {
    backgroundColor: "#FFC0CB",
    height: 200,
  },
  avatar: {
    width: 130,
    height: 130,
    borderRadius: 63,
    borderWidth: 4,
    borderColor: "white",
    marginBottom: 10,
    alignSelf: 'center',
    position: 'absolute',
    marginTop: 130
  },
  name: {
    fontSize: 22,
    color: "#FFFFFF",
    fontWeight: '600',
  },
  body: {
    marginTop: 40,
  },
  bodyContent: {
    flex: 1,
    alignItems: 'center',
    padding: 30,
  },
  name: {
    fontSize: 28,
    color: "#696969",
    fontWeight: "600"
  },
  info: {
    fontSize: 16,
    color: "#00BFFF",
    marginTop: 10
  },
  description: {
    fontSize: 16,
    color: "#696969",
    marginTop: 10,
    textAlign: 'center'
  },
  buttonContainer: {
    marginTop: 10,
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 30,
    backgroundColor: "#FFC0CB",
  },
  icon: {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 40,
    position: 'absolute',
    bottom: 20,
    right: 20,
    height: 40,
    backgroundColor: '#fff',
    borderRadius: 100,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    height: 150,
    width: 300,
    margin: 10,
    backgroundColor: "white",
    borderRadius: 20,
    paddingTop: 15,
    padding: 5,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.5,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    textAlign: 'center',
    margin: 5,
    padding: 10,
    elevation: 2,
    height: 45,
    width: 80
  },
  buttonOpen: {
    backgroundColor: "#F194FF",
  },
  buttonSubmit: {
    backgroundColor: "#FFC0CB",
  },
  buttonClose: {
    backgroundColor: "#d4d2d2",
  },
  inputView: {
    backgroundColor: "#FFC0CB",
    borderRadius: 30,
    width: "70%",
    height: 45,
    marginBottom: 20,

    alignItems: "center",
  },
  TextInput: {
    height: 70,
    flex: 1,
    padding: 5,
    marginLeft: 20,
  },

});
