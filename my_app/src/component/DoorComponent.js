import React from 'react'
import { View, StyleSheet, Text } from 'react-native';
import { Switch } from 'react-native-elements';
import { Icon } from 'react-native-elements'
class DoorComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stateDoor: false
        }
    }
    setStateDoor = () => {
        let data = !this.state.stateDoor
        this.setState({
            ...this.state,
            stateDoor:data
        })
        this.props.addStateDoor({state:data})
    }
    render() {
        return (
            <View style={styles.container}>
                <Icon

                    name='door-open'
                    type='font-awesome-5'
                    color='white'
                    size={60}
                />
                <View style={{height:20}}></View>
                <Switch value={this.state.stateDoor} color="pink" onValueChange={() => { this.setStateDoor() }}></Switch>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        width: 150,
        height: 150,
        backgroundColor: 'rgba(255, 192, 203, 0.5)',
        borderRadius: 30,
        marginTop: 120,
        marginLeft: 30,
        alignItems: 'center',
        paddingTop: 20
    }
})
export default DoorComponent;
