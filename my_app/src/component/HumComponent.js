import React from 'react'
import { LineChart, Grid } from 'react-native-svg-charts'
import * as shape from 'd3-shape'
import { Circle, G, Rect, Text } from 'react-native-svg'
import { Button, View } from 'react-native'
import { StyleSheet } from 'react-native'
import { Icon } from 'react-native-elements'
class HumComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }
    render() {
        let data = [80, 85, 77, 90, 95, 86, 88, 70]
        if (this.props.tempstate.humidity) {
            data = this.props.tempstate.humidity
        }
        const Tooltip = ({ x, y }) => (
            <G
                x={x(7) - (50 / 2)}
                key={'tooltip'}
                onPress={() => console.log('tooltip clicked')}
            >
                <G y={50}>
                    <Rect
                        height={30}
                        width={50}
                        stroke={'grey'}
                        fill={'white'}
                        ry={10}
                        rx={10}
                    />
                    <Text
                        x={50 / 2}
                        dy={15}
                        alignmentBaseline={'middle'}
                        textAnchor={'middle'}
                        stroke={'#2b9eea'}
                    >
                        {`${data[7]} %`}
                    </Text>
                </G>
                <G x={50 / 2}>
                    <Circle
                        cy={y(data[7])}
                        r={6}
                        stroke={'#2b9eea'}
                        strokeWidth={2}
                        fill={'white'}
                    />
                </G>
            </G>
        )

        return (
            <View style={style.container}>
                <Icon
                    reverse
                    name='cloud-outline'
                    type='ionicon'
                    color='#2b9eea'
                />
                <LineChart
                    style={{ height: 200, marginRight: 20, paddingRight: 30 }}
                    data={data}
                    svg={{
                        stroke: '#2b9eea',
                        strokeWidth: 3,
                    }}
                    contentInset={{ top: 20, bottom: 20, right: 25, left: 20 }}
                    curve={shape.curveLinear}
                >
                    <Grid />
                    <Tooltip />
                </LineChart>
            </View>

        )
    }

}
const style = StyleSheet.create({
    container: {
        marginTop: 100
    }
})

export default HumComponent