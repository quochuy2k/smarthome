import React, { useState } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Modal,
    Pressable,
    TextInput,
    Alert,
    ImageBackground
} from 'react-native';
import { Switch } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { delete_home } from '../action';

export default function FromHome(props) {
    console.log("propd...", props.route.params.data);
    let device
    if (props.route.params.data.device) {
        device = props.route.params.data.device.map((element, key) => {
            return (
                <View style={styles.device}>
                      <Text style ={styles.text}>{element.name_device}</Text>
                    <TouchableOpacity
                        style={styles.icon}
                    >
                        <Icon name='cogs' size={30} color='#FFC0CB' />
                    </TouchableOpacity>
                    <Switch style={styles.switch} color="pink" onValueChange={() => {  }}></Switch>
                </View>
            )
        })
    }
    return (
        <View style={styles.container}>
            <ImageBackground
                source={require("../../assets/bg-home2.jpg")}
                style={{ width: 400, height: 800 }}
            >
                <TouchableOpacity
                    style={styles.iconexit}
                    onPress={() => { props.navigation.navigate('House') }}
                >
                    <Icon name='angle-double-left' size={30} color='#FFC0CB' />
                </TouchableOpacity>
                <View style={styles.component}>
                    {device}
                </View>
            </ImageBackground>
        </View>
    )
}
const styles = StyleSheet.create({
    text:{

    },
    switch:{
        marginTop: 70
    },
    container: {
        flex: 1,
        flexDirection: "column",
    },
    component: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        paddingTop: 70
    },
    iconexit: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        position: 'absolute',
        top: 40,
        left: 35,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 100,
    },
    icon: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        position: 'absolute',
        top: 50,
        left: 50,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 100,
    },
    device: {
        width: 150,
        height: 150,
        backgroundColor: 'rgba(255, 192, 203, 0.5)',
        borderRadius: 30,
        marginTop: 50,
        marginLeft: 30,
        alignItems: 'center',
        paddingTop: 20
    }
})