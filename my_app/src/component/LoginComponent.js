import { StatusBar } from "expo-status-bar";
import React, { useEffect, useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button,
    TouchableOpacity,
} from "react-native";

export default function LoginComponent(props) {
    useEffect(() => {
        if (props.user.status==true) {
            props.navigation.navigate('House')
        }
      },[props.user]);    
    const [username, setUserName] = useState("");
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("")
    const [fullName, setFullName] = useState("")
    const [stateLogin, setStateLogin] = useState(true)
    const login = () => {
       let data ={
           user_name:username,
           password:password
       }
       props.login(data)
    }
    const register=()=>{
        let data ={
            user_name:username,
            password:password,
            full_name:fullName,
            email:email
        }
        props.register(data)
    }
    if (stateLogin == true) {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={require("../../assets/home-logo.jpg")} />

                <StatusBar style="auto" />
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Username."
                        placeholderTextColor="#003f5c"
                        onChangeText={(username) => setUserName(username)}
                    />
                </View>

                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Password."
                        placeholderTextColor="#003f5c"
                        secureTextEntry={true}
                        onChangeText={(password) => setPassword(password)}
                    />
                </View>

                <TouchableOpacity onPress={() => setStateLogin(false)}>
                    <Text style={styles.forgot_button}>Register?</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.loginBtn} onPress={()=>{login()}}>
                    <Text style={styles.loginText}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        );
    }
    else {
        return (
            <View style={styles.container}>
                <Image style={styles.image} source={require("../../assets/home-logo.jpg")} />

                <StatusBar style="auto" />
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Username."
                        placeholderTextColor="#003f5c"
                        onChangeText={(username) => setUserName(username)}
                    />
                </View>

                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Password."
                        placeholderTextColor="#003f5c"
                        secureTextEntry={true}
                        onChangeText={(password) => setPassword(password)}
                    />
                </View>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Email."
                        placeholderTextColor="#003f5c"
                        secureTextEntry={false}
                        onChangeText={(email) => setEmail(email)}
                    />
                </View>
                <View style={styles.inputView}>
                    <TextInput
                        style={styles.TextInput}
                        placeholder="Full name."
                        placeholderTextColor="#003f5c"
                        secureTextEntry={false}
                        onChangeText={(fullname) => setFullName(fullname)}
                    />
                </View>

                <TouchableOpacity onPress={() => setStateLogin(true)}>
                    <Text style={styles.forgot_button}>Login?</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.loginBtn} onPress={()=>{register()}}>
                    <Text style={styles.loginText}>REGISTER</Text>
                </TouchableOpacity>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center",
    },

    image: {
        marginBottom: 40,
    },

    inputView: {
        backgroundColor: "#FFC0CB",
        borderRadius: 30,
        width: "70%",
        height: 45,
        marginBottom: 20,

        alignItems: "center",
    },

    TextInput: {
        height: 50,
        flex: 1,
        padding: 10,
        marginLeft: 20,
    },

    forgot_button: {
        height: 30,
        marginBottom: 30,
    },

    loginBtn: {
        width: "80%",
        borderRadius: 25,
        height: 50,
        alignItems: "center",
        justifyContent: "center",
        marginTop: 40,
        backgroundColor: "#FF1493",
    },
});