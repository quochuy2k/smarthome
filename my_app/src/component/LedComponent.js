import React from 'react'
import { View, StyleSheet, Text } from 'react-native';
import { Switch } from 'react-native-elements';
import { Icon } from 'react-native-elements'
class LedComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            stateLedRed: false,
            stateLedGreen:false,
            stateLedBlue:false
        }
    }
    setStateLed = (type) => {
        let color = ''
        let data
        if (type == 1) {
            color = 'red'
            data = !this.state.stateLedRed
            this.setState({
                ...this.state,
                stateLedRed: data
            })
        }
        else if (type == 2) {
            color = 'green'
            data = !this.state.stateLedGreen
            this.setState({
                ...this.state,
                stateLedGreen: data
            })
        } else {
            color = 'yellow'
            data = !this.state.stateLedBlue
            this.setState({
                ...this.state,
                stateLedBlue: data
            })
        }
        this.props.addStateLed({ state: data+'/'+color })
    }
    render() {
        return (
            <View style={styles.components}>
                <View >
                     <Icon
                        name='lightbulb-o'
                        type='font-awesome'
                        color='red'
                        size={70}
                    />
                    <View style={{ height: 20 }}></View>
                    <Switch value={this.state.stateLedRed} color="pink" onValueChange={() => { this.setStateLed(1) }}></Switch>
                </View>
                <View>
                     <Icon
                        name='lightbulb-o'
                        type='font-awesome'
                        color='green'
                        size={70}
                    />
                    <View style={{ height: 20 }}></View>
                    <Switch value={this.state.stateLedGreen} color="pink" onValueChange={() => { this.setStateLed(2) }}></Switch>
                </View>     
                <View>
                    <Icon
                        name='lightbulb-o'
                        type='font-awesome'
                        color='yellow'
                        size={70}
                    />
                    <View style={{ height: 20 }}></View>
                    <Switch value={this.state.stateLedBlue} color="pink" onValueChange={() => { this.setStateLed(3) }}></Switch> 
                </View>         
 
            </View>

        )
    }
}
const styles = StyleSheet.create({
    components: {
        width: 150,
        height: 150,
        
        backgroundColor: 'rgba(255, 192, 203, 0.5)',
        borderRadius: 30,
        marginTop: 120,
        marginLeft: 30,
        alignItems: 'center',
        paddingTop: 20,
        flexDirection: 'row',
        flexWrap: 'wrap',
    }
})
export default LedComponent;
