import React from 'react'
import TempContainer from '../container/TempPageContainer'
class TempScreen extends React.Component {
    render() {
        return (
            <TempContainer {...this.props} />
        )
    }
}
export default TempScreen