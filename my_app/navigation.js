import * as React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from './src/screen/HomeScreen'
import TempScreen from './src/screen/TempScreen'
import LoginContainer from './src/container/LoginContainer'
import HouseContainer from './src/container/HouseContainer'
import FormComponent from './src/component/FormHomeComponent'
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
function MyDrawer() {
    return (
        <Drawer.Navigator
            drawerStyle={{
                backgroundColor: '#FFC0CB',
                width: 200,
            }}
        >
            <Drawer.Screen name="Home Screen" component={HomeScreen} />
            <Drawer.Screen name="Temperature" component={TempScreen} />
        </Drawer.Navigator>
    );
}

export default function Navigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login"
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen name="Login" component={LoginContainer}
                />
                <Stack.Screen name="Home" component={MyDrawer} />
                <Stack.Screen name="House" component={HouseContainer}/>
                <Stack.Screen name="Form" component={FormComponent}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}